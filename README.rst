Diagramas do Draw
=================
 |Diagrama|

Repositório de diagramas do draw io.

 |docs| |python| |license| |github|


:Author:  Carlo E. T. Oliveira
:Version: 22.08
:Affiliation: Universidade Federal do Rio de Janeiro
:License: GNU General Public License v3 or later (GPLv3+)
:Homepage: `Em Construção`_
:Changelog: `CHANGELOG <CHANGELOG.rst>`_

Diagramas
---------

Diagramas gerados com o Draw criados em projetos do LABASE.

-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**

LABASE_ - NCE_ - UFRJ_

|LABASE|

.. _LABASE: http://labase.activufrj.nce.ufrj.br
.. _NCE: http://nce.ufrj.br
.. _UFRJ: http://www.ufrj.br

.. _Em Construção: https://i.imgur.com/MjJUb0f.jpg

.. |github| image:: https://img.shields.io/badge/release-21.02-blue
   :target: https://gitlab.com/cetoli/eucaros/-/releases


.. |LABASE| image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :target: http://labase.activufrj.nce.ufrj.br
   :alt: LABASE

.. |Diagrama| image:: https://i.imgur.com/hbNLRd6.png
   :alt: DIAGRAMA
   :width: 800px

.. |python| image:: https://img.shields.io/github/languages/top/kwarwp/kwarwp
   :target: https://www.python.org/downloads/release/python-383/

.. |docs| image:: https://img.shields.io/readthedocs/supygirls
   :target: https://supygirls.readthedocs.io/en/latest/index.html

.. |license| image:: https://img.shields.io/github/license/kwarwp/kwarwp
   :target: https://gitlab.com/cetoli/eucaros/-/raw/master/LICENSE
